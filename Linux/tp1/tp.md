# TP1 : Are you dead yet ?

🌞 **Trouver au moins 5 façons différentes de péter la machine**

## Première méthode : "Tout en finesse"

* Pour cette méthode, prérequis : 
    ➜ Une batte de baseball
        ➜ Votre victime : **La VM**
        
Mise en situation : Tu es en pleine session de classées sur ton **jeu préféré**. **Le jeu** qui renforce des amitiés, qui nous fait passer du temps avec notre famille, ce jeu qui ne nous donnera jamais envie de fracasser le mur. J'ai nommé : ***League of Legends***.
Depuis le début, tu es un énorme looser. Tu perds absolument toutes tes games. Tu tombes constamment avec une équipe nulle qui ne sait pas jouer. Et là; miracle ! Une très bonne composition d'équipe. Vous commencez alors à valider votre choix de personnages. Evidemment tu valides ton main. Et en face de toi, alors que jusqu'à présent le choix des personnages de l'équipe adverse puait la mort. Il y a...lui. Xx_Tr0ll3r40_xX. Et qui a t'il choisis à ton avis ? **Teemo**.

Solution : Tu prends ta meilleure batte de baseball et tu mets des bons gros coups dans ta tour jusqu'à ce qu'il n'en reste **plus rien**.

Point positif : Pas besoin de code pour casser la VM. Une méthode délicate et très efficace. Et puis l'accès physique c'est la première étape, c'est ce que tu m'as dis. Tu m'as aussi dis qu'il ne fallait pas mettre de prendre un marteau et de taper la tour avec. Mais avec une batte, ça passe !


## Deuxième méthode : "Et hop! Aux oubliettes !"

Les fichiers de configurations ? C'est **CIAO!**

* Prérequis : 
    ➜ Les droits d'administrateurs

Objectif de la méthode : Je souhaite faire en sorte que la machine ne boot plus. Je vais utiliser la commande suivante : `sudo mv /etc /null`

* sudo : permet d'utiliser les droits root sur la commande
* mv : la commande move qui permet de déplacer un fichier/dossier à une autre destination
* /etc : dossier qui comporte tout les fichiers de configuration de la VM
* /null : fichier qui efface toutes les données que tu mets à l'intérieur

La commande déplace tout les fichiers qui servent à configurer la machine dans le fichier /null qui effacera tout les fichiers de configuration.

Résultat : La machine est actuellement encore en vie. Cependant, il n'y a plus aucune possibilité d'utiliser de commandes. Au prochain redémarrage de la machine, elle ne bootera plus parce qu'elle ne retrouvera plus les fichiers nécessaires. (données user, boot, mot de passe, commande, etc...)


## Troisième méthode : "Vive l'aléatoire !"

La subtilité est de mise.

* Prérequis : 
    ➜ Mot de passe root
        Par défaut, ubuntu ne donne pas de mot de passe root. Il faut donc le configurer avec la commande `sudo passwd root`

Objectif de la méthode : Je veux que ma machine ne boot plus. Mais cette fois-ci c'est un petit peu plus vicieux. Voici ma commande : `cat /dev/urandom >/dev/sda`

* cat : affiche les données
* /dev/urandom : fichier servant à créer des nombres aléatoires
* ">" : sert à afficher les nombres aléatoires dans le fichier de destination
* /dev/sda : fichier de destination contenant toutes les informations sur les devices

La commande va ajouter des nombres aléatoires à chaque ligne du fichier /dev/sda.

Résultat : La machine crash et il sera impossible de la redémarrer. Elle affichera un message d'erreur "FATAL : No bootable medium found! System halted."


## Quatrième méthode : "Poof."

La base, t'as du la voir dans 120 TP mais c'est toujours mieux quand c'est moi qui fais ! :D

Pas de prérequis pour une fois.

Objectif de la méthode : toukacé

`rm -rf /*`

* rm : remove soit supprimer un fichier/dossier
* "-" : rajouter une option à la commande
* rf : recursive forced soit réexcuter la commande en en forçant son exécution tant que c'est possible
* /* : l'ensemble des dossiers de la VM

La commande va supprimer tout les fichiers de la VM avec les fichiers de configurations, etc... .
*(Je sais qu'elle est basique mais j'ai adoré l'effet visuel.)*

Résultat : La machine crash, impossible de la reboot. Parce que de toute façon il n'y a plus rien dedans lol


## Cinquième méthode : "VA AU COIN !"

On punit la VM parce que c'est une sale gosse.

* Prérequis : 
     ➜ Les droits sudo

Objectif de la méthode : Retirer les permissions d'initialisation de la VM.

`sudo chmod 000 /usr/sbin/init`

* chmod : permet de changer les permissions d'un fichier (lecture, écriture, exécution) chmod 000 -> enlève toutes les permissions
* /usr/sbin/init : fichier d'initialisation qui se lance au démarrage et lance tout les autres programmes

La commande va enlever toutes les permissions du fichier.

Résultat : On peut toujours utiliser des commandes mais dès le prochain reboot, **she will be dead**.

![ARE U DED YET](./pics/dead-yet.gif)

