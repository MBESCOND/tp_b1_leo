# TP2 : Manipulation de services

Dans nos cours, on ne va que peu s'attarder sur l'aspect client des systèmes GNU/Linux, mais plutôt sur la façon dont on le manipule en tant qu'admin.

Ca permettra aussi, *via* la manipulation, d'appréhender un peu mieux comment un OS de ce genre fonctionne.

# Intro

Dans ce TP on va s'intéresser aux *services* de la machine. Un *service* c'est un processus dont l'OS s'occupe. 

Plutôt que de le lancer à la main, on demande à l'OS de le gérer, c'est moins chiant !

> **Par exemple, quand vous ouvrez votre PC, vous lancez pas une commande pour avoir une interface graphique si ?** L'interface graphique, elle est juste là, elle pop "toute seule". En vérité, c'est l'OS qui l'a lancée. L'interface graphique est donc un *service*.

Souvent un service...

- bon bah c'est un processus qui s'exécute
  - c'est le système qui a fait en sorte qu'il se lance
  - le système a la charge du processus
  - genre il va le relancer si le processus crash par exemple
- souvent il a un fichier de configuration
  - ça nous permet de le paramétrer
  - les changements prennent effet quand on redémarre le service
- souvent on peut définir sous quelle identité le service va tourner
  - genre on désigne un utilisateur du système qui lancera le processus
  - c'est cet utilisateur qui s'affichera dans la liste des processus
- si c'est un service qui utilise le réseau (comme SSH, HTTP, FTP, autres.) il écoute sur un port

# Prérequis

> Y'a toujours une section prérequis dans mes TPs, ça vous sert à préparer l'environnement pour réaliser le TP. Dans ce TP, c'est le premier avec des prérequis, alors je vais détailler le principe et vous devrez me rendre la réalisation de ces étapes dans le compte rendu.

## 1. Une machine xubuntu fonctionnelle

N'hésitez pas à cloner celle qu'on a créé ensemble.

**Pour TOUTES les commandes que je vous donne dans le TP**

- elles sont dans le [memo de commandes Linux](../../cours/memos/commandes.md)
- vous **DEVEZ** consulter le `help` au minimum voire le `man` ou faire une recherche Internet pour comprendre comment fonctionne la commande

```bash
# Consulter le help de ls
$ ls --help

# Consulter le manuel de ls
$ man ls
```

## 2. Nommer la machine

➜ **On va renommer la machine**

- parce qu'on s'y retrouve plus facilement
- parce que toutes les machines sont nommées dans la vie réelle, pour cette raison, alors habituez vous à le faire systématiquement :)

On désignera la machine par le nom `node1.tp2.linux`

🌞 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**
  - taper la commande `sudo hostname <NOM_MACHINE>`
  `sudo hostname node1.tp2.linux`
  - pour vérifier le changement, vous pouvez ouvrir un nouveau terminal, et observer le changement dans le prompt du terminal 
  `erya@node1`
- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**
  - il faut inscrire le nom de la machine dans le fichier `/etc/hostname`
  `sudo nano /etc/hostname`
  `cat /etc/hostname`
  `node1.tp2.linux`
  - pour vérifier le changement, il faut redémarrer la machine
- je veux les deux étapes dans le compte-rendu

## 3. Config réseau

➜ **Vérifiez avant de continuer le TP que la configuration réseau de la machine est OK. C'est à dire :**

- la machine doit pouvoir joindre internet
- votre PC doit pouvoir `ping` la machine

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash
# Affichez la liste des cartes réseau de la machine virtuelle
# Vérifiez que les cartes réseau ont toute une IP
$ ip a

# Si les cartes n'ont pas d'IP vous pouvez les allumez avec la commande
$ nmcli con up <NOM_INTERFACE>
# Par exemple
$ nmcli con up enp0s3

# Vous devez repérer l'adresse de la VM dans le host-only

# Vous pouvez tester de ping un serveur connu sur internet
# On teste souvent avec 1.1.1.1 (serveur DNS de CloudFlare)
# ou 8.8.8.8 (serveur DNS de Google)
$ ping 1.1.1.1

# On teste si la machine sait résoudre des noms de domaine
$ ping ynov.com
```

Ensuite on vérifie que notre PC peut `ping` la machine (étapes à réaliser SUR VOTRE PC) :

```bash
# Afficher la liste de vos carte réseau, la commande dépend de votre OS
$ ip a # Linux
$ ipconfig # Windows
$ ifconfig # MacOS

# Vous devez repérer l'adresse de votre PC dans le host-only

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>
```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  - depuis la VM : `ping 1.1.1.1` fonctionnel
  `ping 1.1.1.1`
```
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=55 time=15.4 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=55 time=53.7 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=55 time=16.1 ms
[...]
--- 1.1.1.1 ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7012ms
rtt min/avg/max/mdev = 14.262/21.185/53.658/12.565 ms`
  - depuis la VM : `ping ynov.com` fonctionnel
  `ping ynov.com`
  `PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=15.4 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=50 time=13.9 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=50 time=15.9 ms
[...]
--- ynov.com ping statistics ---
11 packets transmitted, 11 received, 0% packet loss, time 10014ms
rtt min/avg/max/mdev = 13.921/15.900/18.081/1.277 ms
```
  - depuis votre PC : `ping <IP_VM>` fonctionnel
 `ping 10.1.1.4`
```
Envoi d’une requête 'Ping'  10.1.1.4 avec 32 octets de données :
Réponse de 10.1.1.4 : octets=32 temps<1ms TTL=64
Réponse de 10.1.1.4 : octets=32 temps<1ms TTL=64
Réponse de 10.1.1.4 : octets=32 temps<1ms TTL=64
Réponse de 10.1.1.4 : octets=32 temps<1ms TTL=64
Statistiques Ping pour 10.1.1.4:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
# Go next

Vous pouvez ensuite vous attaquer aux 3 parties du TP. Elles sont indépendantes mais je vous recommande de les faire dans l'ordre.

> Si ce que je vous demande de faire vous paraît flou, difficile, incompréhensible, si vous savez pas par où commencer ou comment vous y prendre. DEMANDEZ-MOI JE SUIS LA POUR CA. :)

# Partie 1 : SSH
# I. Intro

> *SSH* c'est pour *Secure SHell*.

***SSH* est un outil qui permet d'accéder au terminal d'une machine, à distance, en connaissant l'adresse IP de cette machine.**

*SSH* repose un principe de *client/serveur* :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 22/TCP par convention
- le *client*
  - connaît l'IP du serveur
  - se connecte au serveur à l'aide d'un programme appelé "*client* *SSH*"

Pour nous, ça va être l'outil parfait pour contrôler toutes nos machines virtuelles.

Dans la vie réelle, *SSH* est systématiquement utilisé pour contrôler des machines à distance. C'est vraiment un outil de routine pour tout informaticien, qu'on utilise au quotidien sans y réfléchir.

> *Si vous louez un serveur en ligne, on vous donnera un accès SSH pour le manipuler la plupart du temps.*

![Feels like a hacker](./pics/feels_like_a_hacker.jpg)

# II. Setup du serveur SSH

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

Sur les OS GNU/Linux, les installations se font à l'aide d'un gestionnaire de paquets.

🌞 **Installer le paquet `openssh-server`**

- avec une commande `apt install`
`sudo apt install openssh-server`
---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `ssh`
- un dossier de configuration `/etc/ssh/`

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

- avec une commande `systemctl start`
`systemctl start ssh`
- vérifier que le service est actuellement actif avec une commande `systemctl status`
`systemctl status ssh`
```
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 16:03:40 CET; 23min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 1654 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 2.7M
     CGroup: /system.slice/ssh.service
             └─1654 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```

> Vous pouvez aussi faire en sorte que le *service* SSH se lance automatiquement au démarrage avec la commande `systemctl enable ssh`.

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status`
  `systemctl status ssh`
```
ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 16:03:40 CET; 24min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 1654 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 2.7M
     CGroup: /system.slice/ssh.service
             └─1654 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```
- afficher le/les processus liés au *service* `ssh`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
 `ps -ef`
```
root        1654       1  0 16:03 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root        2789    1654  0 16:07 ?        00:00:00 sshd: erya [priv]
erya        2867    2789  0 16:07 ?        00:00:00 sshd: erya@pts/1`
- afficher le port utilisé par le *service* `ssh`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
 `ss -lntr`
```
```
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
[...]
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
LISTEN        0             5                    ip6-localhost:631                        [::]:*
LISTEN        0             128                           [::]:22                         [::]:*
```
- afficher les logs du *service* `ssh`
  - avec une commande `journalctl`
  - en consultant un dossier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs
  `journalctl -xe -u ssh`
```
[...]
nov. 08 16:03:40 node1.tp2.linux sshd[1654]: Server listening on 0.0.0.0 port 22.
nov. 08 16:03:40 node1.tp2.linux sshd[1654]: Server listening on :: port 22.
nov. 08 16:03:40 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
```

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

> *La commande `ssh` de votre terminal, c'est un client SSH.*


`ssh erya@10.1.1.4`

```
The authenticity of host '10.1.1.4 (10.1.1.4)' can't be established.
ECDSA key fingerprint is SHA256:BNgY+wCd/d/MOgg0tXYd3zkXgmXP+/Wh4oTKxgT00dk.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.1.1.4' (ECDSA) to the list of known hosts.
erya@10.1.1.4's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
[...]
```
## 4. Modification de la configuration du serveur

Pour modifier comment un *service* se comporte il faut modifier le fichier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
  - modifiez-le comme vous voulez, je vous conseille d'utiliser `nano` en ligne de commande
 `sudo nano /etc/ssh/sshd_config`
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
 `cat /etc/ssh/sshd_config`
 `[...] Port 1026 [...]`
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute
```
ss -lntr
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             5                        localhost:631                     0.0.0.0:*
LISTEN        0             128                        0.0.0.0:1026                    0.0.0.0:*
LISTEN        0             4096                  localhost%lo:53                      0.0.0.0:*
LISTEN        0             5                    ip6-localhost:631                        [::]:*
LISTEN        0             128                           [::]:1026                       [::]:*
```
> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*
`ssh -p 1026 erya@10.1.1.4`
`Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)[...]`



# Partie 2 : FTP

# I. Intro

> *FTP* c'est pour *File Transfer Protocol*.

***FTP* est un protocole qui permet d'envoyer simplement des fichiers sur un serveur à travers le réseau.**

*FTP* repose un principe de client/serveur :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 21/TCP par convention
- le *client*
  - connaît l'IP du *serveur*
  - se connecte au *serveur* à l'aide d'un programme appelé "*client FTP*"

Dans la vie réelle, *FTP* est souvent utilisé pour échanger des fichiers avec un serveur de façon sécurisée. En vrai ça commence à devenir oldschool *FTP*, mais c'est un truc très basique et toujours très utilisé.

> Si vous louez un serveur en ligne, on vous donnera parfois un accès *FTP* pour y déposer des fichiers.

# II. Setup du serveur FTP

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `vsftpd`
- un fichier de configuration `/etc/vsftpd.conf`

> Le paquet s'appelle `vsftpd` pour *Very Secure FTP Daemon*. A l'apogée de l'utilisation de FTP, il n'était pas réputé pour être un protocole très sécurisé. Aujourd'hui, avec des outils comme `vsftpd`, c'est bien mieux qu'à l'époque.

```
sudo apt install vsftpd

nc -l -p 1029 >lognc
nc localhost 1029
hey
ls
Desktop  Documents  Downloads  linux.txt  lognc  Music  Pictures  Public  Templates  toto.odt  Videos
cat lognc
hey
```
## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

- avec une commande `systemctl start`
`systemctl start vsftpd`
- vérifier que le service est actuellement actif avec une commande `systemctl status`
`systemctl status vsftpd`
```
vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 16:53:40 CET; 6min ago
   Main PID: 3256 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 536.0K
     CGroup: /system.slice/vsftpd.service
             └─3256 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 16:53:40 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 16:53:40 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

> Vous pouvez aussi faire en sorte que le service FTP se lance automatiquement au démarrage avec la commande `systemctl enable vsftpd`.

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  - avec une commande `systemctl status`
 `systemctl status vsftpd`
 ```
 ● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 16:53:40 CET; 7min ago
   Main PID: 3256 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 536.0K
     CGroup: /system.slice/vsftpd.service
             └─3256 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 08 16:53:40 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 16:53:40 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
- afficher le/les processus liés au service `vsftpd`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
  `ps -ef`
  `[...]
root        3256       1  0 16:53 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd.conf
[...]`
- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
  `ss -lntr`
  `[...]
LISTEN        0             32                               *:21                            *:*`
- afficher les logs du service `vsftpd`
  - avec une commande `journalctl`
  - en consultant un fichier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs
  `journalctl -xe -u vsftpd`
  `[...]
nov. 08 16:53:40 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
[...]`

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
  - les navigateurs Web, ils font ça maintenant
  - demandez moi si vous êtes perdus
- essayez d'uploader et de télécharger un fichier
  - montrez moi à l'aide d'une commande la ligne de log pour l'upload, et la ligne de log pour le download
- vérifier que l'upload fonctionne
  - une fois un fichier upload, vérifiez avec un `ls` sur la machine Linux que le fichier a bien été uploadé

> Par défaut, `vsftpd` n'autorise pas les uploads. Il faut modifier son fichier de configuration pour qu'il les accepte.
Hint : un upload, ça correspond à une écriture sur le serveur, on écrit des données sur le serveur (ou *write* en anglais).

`sudo nano /etc/vsftpd.conf`
`cat /etc/vsftpd.conf`
```
write_enable=YES
anon_upload_enable=YES

ftp 10.1.1.4
put C:\Users\33625\Documents\Code\linux.txt
200 PORT command successful. Consider using PASV.
150 Ok to send data.
226 Transfer complete.
ftp : 5 octets envoyés en 0.00 secondes à 2.50 Ko/s.
ftp> get /home/erya/toto.odt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for /home/erya/toto.odt (8308 bytes).
226 Transfer complete.
ftp : 8308 octets reçus en 0.00 secondes à 8308000.00 Ko/s.
ls
Desktop  Documents  Downloads  linux.txt  Music  Pictures  Public  Templates  toto.odt  Videos
```

🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
- mettez en évidence une ligne de log pour un upload

> Par défaut, `vsftpd` ne génère pas de ligne dans les logs quand on upload ou download un fichier.  
Pour qu'il le fasse, il faut modifier à nouveau le fichier de configuration de `vsftpd`, et activer le `xfer_log`. Je vous laisse chercher ça directement dans le fichier de conf, ou sur Google c:

```
sudo cat /var/log/vsftpd.log
[...]
Mon Nov  8 17:19:06 2021 [pid 3963] [erya] OK UPLOAD: Client "::ffff:10.1.1.1", "/home/erya/linux.txt", 5 bytes, 0.00Kbyte/sec
Mon Nov  8 17:22:26 2021 [pid 3963] [erya] OK DOWNLOAD: Client "::ffff:10.1.1.1", "/home/erya/toto.odt", 8308 bytes, 16357.42Kbyte/sec
```

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
  `cat /etc/vsftpd.conf`
  `listen_port=1048`
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute
```
ss -lntr
  State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
[...]
LISTEN        0             32                               *:1048                          *:*
[...]
```
> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*
- re-tester l'upload et le download

`open 10.1.1.4 1048`
```
Connecté à 10.1.1.4.
220 (vsFTPd 3.0.3)
200 Always in UTF8 mode.
Utilisateur (10.1.1.4:(none)) : erya
331 Please specify the password.
Mot de passe :
230 Login successful.
ftp> put C:\Users\33625\Documents\Code\linux.txt
200 PORT command successful. Consider using PASV.
150 Ok to send data.
226 Transfer complete.
ftp : 5 octets envoyés en 0.00 secondes à 5.00 Ko/s.
ftp> get /home/erya/toto.odt
200 PORT command successful. Consider using PASV.
150 Opening BINARY mode data connection for /home/erya/toto.odt (8308 bytes).
226 Transfer complete.
ftp : 8308 octets reçus en 0.00 secondes à 8308000.00 Ko/s.
```

```
sudo cat /var/log/vsftpd.log
[...]
Mon Nov  8 17:36:33 2021 [pid 4122] [erya] OK UPLOAD: Client "::ffff:10.1.1.1", "/home/erya/linux.txt", 5 bytes, 2.79Kbyte/sec
Mon Nov  8 17:37:13 2021 [pid 4122] [erya] OK DOWNLOAD: Client "::ffff:10.1.1.1", "/home/erya/toto.odt", 8308 bytes, 16161.91Kbyte/sec
```



# Partie 3 : Création de votre propre service

- [Partie 3 : Création de votre propre service](#partie-3--création-de-votre-propre-service)
- [I. Intro](#i-intro)
- [II. Jouer avec netcat](#ii-jouer-avec-netcat)
- [III. Un service basé sur netcat](#iii-un-service-basé-sur-netcat)
  - [1. Créer le service](#1-créer-le-service)
  - [2. Test test et retest](#2-test-test-et-retest)

# I. Intro

Comme on l'a dit plusieurs fois plus tôt, un *service* c'est juste un processus que l'on demande au système de lancer. Puis il s'en occupe.

Ainsi, il nous faut juste trouver comment, dans Linux, on fait pour définir un nouveau *service*. On aura plus qu'à indiquer quel processus on veut lancer.

Histoire d'avoir un truc un minimum tangible, et pas juste un service complètement inutile, on va apprendre un peu à utiliser `netcat` avant de continuer.

`netcat` est une commande très simpliste qui permet deux choses :

- **écouter sur un port** réseau, et attendre la connexion de clients
  - on parle alors d'un `netcat` qui agit comme un serveur
- **se connecter sur un port** d'un serveur dont on connaît l'IP
  - on parle alors d'un `netcat` qui agit comme un client

Dans un premier temps, vous allez utiliser `netcat` à la main et jouer un peu avec. On peut fabriquer un outil de discussion, un chat, assez facilement avec `netcat`. Un chat entre deux machines connectées sur le réseau !

Ensuite, vous créerez un service basé sur `netcat` qui permettra d'écrire dans un fichier de la machine, à distance.

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

# II. Jouer avec netcat

Si vous avez compris la partie I avec SSH et la partie II avec FTP, c'est toujours le même principe : un serveur qui attend des connexions, et un client qui s'y connecte. Le principe :

- la VM va agir comme un serveur, à l'aide de la commande `netcat`
  - ce sera une commande `nc -l`
  - le `-l` c'est pour `listen` : le serveur va écouter
  - il faudra préciser un port sur lequel écouter
- votre PC agira comme le client
  - il faudra avoir la commande `nc` dans le terminal de votre PC
  - ce sera une commande `nc` (sans le `-l`) pour se connecter à un serveur
  - il faudra préciser l'IP et le port où vous voulez vous connecter (IP et port de la VM)

> Sur Windows, la commande s'appelle souvent `ncat.exe` ou `nc.exe`.

Une fois la connexion établie, vous devrez pouvoir échanger des messages entre les deux machines, comme un petit chat !

🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**

- la commande tapée sur la VM
- la commande tapée sur votre PC

```
nc -l -p 1029
nc localhost 1029
n'importe quoi
blbl
trop génial
```

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

- utiliser le caractère `>` et/ou `>>` sur la ligne de commande `netcat` de la VM
- cela permettra de stocker les données échangées dans un fichier
- plutôt que de les afficher dans le terminal
- parce quuueeee pourquoi pas ! Ca permet de faire d'autres trucs avec

Le caractère `>` s'utilise comme suit :

```bash
# On liste les fichiers et dossiers
$ ls
Documents Images

# Le caractère > permet de rediriger le texte dans un fichier
# Plutôt que de l'afficher dans un terminal
$ ls > toto
# Notez l'absence de texte en retour de la commande ls

# Voyons le résultat d'une simple commande ls désormais :
$ ls
Documents Images toto
# Un nouveau fichier toto a été créé

# Regardons son contenu
$ cat toto
Document Images
# Il contient du texte : le résultat de la commande ls
```

> N'hésitez pas à vous entraîner à utiliser `>` et `>>` sur la ligne de commande avant de vous lancer dans cette partie. Je vous laisse Google pour voir la différence entre les deux.

Il sera donc possible de l'utiliser avec `netcat` comme suit :

```bash
$ nc -l IP PORT > YOUR_FILE
```

Ainsi, le fichier `YOUR_FILE` contiendra tout ce que le client aura envoyé vers le serveur avec `netcat`, plutôt que ça s'affiche juste dans le terminal.

> Renommez le fichier `YOUR_FILE` comme vous l'entendez évidemment.
``` 
nc -l -p 1029 > lognc
```
``` 
nc localhost 1029
hey
```
```
ls
Desktop  Documents  Downloads  linux.txt  lognc  Music  Pictures  Public  Templates  toto.odt  Videos
cat lognc
hey

```


# III. Un service basé sur netcat

**Pour créer un service sous Linux, il suffit de créer un simple fichier texte.**

Ce fichier texte :

- a une syntaxe particulière
- doit se trouver dans un dossier spécifique

Pour essayer de voir un peu la syntaxe, vous pouvez utilisez la commande `systemctl cat` sur un service existant. Par exemple `systemctl cat sshd`.

DON'T PANIC pour votre premier service j'vais vous tenir la main.

La commande que lancera votre service sera un `nc -l` : vous allez donc créer un petit chat sous forme de service ! Ou presque hehe.

## 1. Créer le service

🌞 **Créer un nouveau service**

- créer le fichier `/etc/systemd/system/chat_tp2.service`
- définissez des permissions identiques à celles des aux autres fichiers du même type qui l'entourent
- déposez-y le contenu suivant :

```bash
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=<NETCAT_COMMAND>

[Install]
WantedBy=multi-user.target
```

Vous devrez remplacer `<NETCAT_COMMAND>` par une commande `nc` de votre choix :

- `nc` doit écouter, listen (`-l`)
- et vous devez préciser le chemin absolu vers cette commande `nc`
  - vous pouvez taper la commande `which nc` pour connaître le dossier où se trouve `nc` (son chemin absolu)

**Il faudra exécuter la commande `sudo systemctl daemon-reload` à chaque fois que vous modifiez un fichier `.service`.**

```
sudo nano /etc/systemd/system/chat_tp2.service
erya@node1:/etc/systemd/system$ sudo chmod 777 chat_tp2.service
erya@node1:/etc/systemd/system$ ls -al
[...]
-rwxrwxrwx  1 root root  127 nov.   8 18:00 chat_tp2.service
```

## 2. Test test et retest

🌞 **Tester le nouveau service**

- depuis la VM
  - démarrer le nouveau service avec une commande `systemctl start`

  `sudo systemctl start chat_tp2.service`
  - vérifier qu'il est correctement lancé avec une commande  
`systemctl status`

```
  erya@node1:/etc/systemd/system$ sudo systemctl status chat_tp2.service
  ● chat_tp2.service - Little chat service (TP2)
  ```
  ```
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 18:09:42 CET; 17s ago
   Main PID: 4378 (nc)
      Tasks: 1 (limit: 2312)
     Memory: 184.0K
     CGroup: /system.slice/chat_tp2.service
             └─4378 /usr/bin/nc -l -p 1029
```
  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi
```
  ss -lntr
  State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             1                          0.0.0.0:1029                    0.0.0.0:*
```
- tester depuis votre PC que vous pouvez vous y connecter
 ```
nc localhost 1029
 yo
 ```
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :

```bash
# Voir l'état du service, et les derniers logs
$ systemctl status chat_tp2

# Voir tous les logs du service
$ journalctl -xe -u chat_tp2

# Suivre en temps réel l'arrivée de nouveaux logs
# -f comme follow :)
$ journalctl -xe -u chat_tp2 -f
```
```
journalctl -xe -u chat_tp2`
`[...]
nov. 08 18:15:48 node1.tp2.linux nc[4378]: yo
nov. 08 18:15:49 node1.tp2.linux systemd[1]: chat_tp2.service: Succeeded.
[...]
```
