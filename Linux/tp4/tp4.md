🌞 **Choisissez et définissez une IP à la VM**

```console
[morgane@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
NAME=enp0s8
DEVICE=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.1.1.3
NETMASK=255.255.255.0

[morgane@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
  link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
  inet 127.0.0.1/8 scope host lo
  valid_lft forever preferred_lft forever
  inet6 ::1/128 scope host
  valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
  link/ether 08:00:27:b1:6d:3c brd ff:ff:ff:ff:ff:ff
  inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
  valid_lft 82340sec preferred_lft 82340sec
  inet6 fe80::a00:27ff:feb1:6d3c/64 scope link noprefixroute
  valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
  link/ether 08:00:27:7d:d4:f3 brd ff:ff:ff:ff:ff:ff
  inet 10.1.1.3/24 brd 10.1.1.255 scope global noprefixroute enp0s8
  valid_lft forever preferred_lft forever
  inet6 fe80::a00:27ff:fe7d:d4f3/64 scope link
  valid_lft forever preferred_lft forever
[morgane@localhost ~]$
```

🌞 **Vous me prouverez que :**

```
[morgane@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
Active: active (running) since Tue 2021-11-23 16:03:31 CET; 3h 27min ago
Docs: man:sshd(8)
man:sshd_config(5)
Main PID: 26571 (sshd)
Tasks: 1 (limit: 4934)
Memory: 7.0M
CGroup: /system.slice/sshd.service
└─26571 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-c>


[morgane@localhost ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDICXSonuvggHup531IoKhllw4i94Wy6rzV+wwchX+zcx9/SEgiVQPjplfnb3aD3K07xFuGgUVdEBzvKSmryN82nDtXmXV2+PSId6C4cDtBja5fruAYrNdM5pw9fBWUQ5D+BInRPECHuKbLl0fpJoaXa0ooVnkWADW5UTdcCJI8YmFoUyzRSB/zB1GrV91nN5A42GV/oq+maGB1IE5HLhdSjQUvjoQBgu6Ady9RTYGAl9WFZ6lxPdkAh2hLzMT5WgXlZyPTQXJ6kTtFWUs2U6jJWwGheN+CzFWtFuhu5zovmsaE/dU78xZBCqfhniCqxq/yzrEPy9nvGrh3VxT5H/DPZR7YdZvj0XuSBrUoHvlv9DFi8ZqDWJd0ZuayhTWXGvVs8fZG2D3XUx/vahv1ZV9Hu1RNu0tg5vtXl7XJOElkANcwP8yMoNWx4e+yQKrVakWFEb7mBrIwzMYXD9Q4ziZsQAxZInFcKnl3NfhWckm0sDSLO6x55fkPw5FewzhG8sjDGYFjwUB3fUm65h2UofdZrOGqnSx21rfkY3SWgslM+uhUtFfwsWifUfotUkktOJl5wXndpIJO5YL7EfuaAdOv8qMTf1EiRtPZgFBtBfnqOMCvBeqj1iU9svSWGsce2gCbfADw9iGJ5wkO09fB7rEBPcTVndG7uh55akhZgT0AfQ== 33625@LAPTOP-SQS0VRPC
```

🌞 **Prouvez que vous avez un accès internet**

```
[morgane@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=14.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=17.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=17.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=14.7 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=113 time=16.3 ms
```

🌞 **Prouvez que vous avez de la résolution de nom**

```
ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=1 ttl=113 time=14.8 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=2 ttl=113 time=15.1 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=3 ttl=113 time=14.1 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=4 ttl=113 time=13.10 ms
```

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

```
[morgane@localhost ~]$ [morgane@localhost ~]$ cat /etc/hostname
node1.tp4.linux
```

# III. Mettre en place un service

🌞 **Installez NGINX en vous référant à des docs online**

```
[morgane@localhost ~]$ sudo dnf update
Last metadata expiration check: 0:02:36 ago on Wed 24 Nov 2021 05:00:10 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
[morgane@localhost ~]$ sudo dnf install nginx
Last metadata expiration check: 0:02:55 ago on Wed 24 Nov 2021 05:00:10 PM CET.
Dependencies resolved.
============================================================================================================================================================================================================================================= Package                                                         Architecture                               Version                                                                      Repository                                     Size =============================================================================================================================================================================================================================================Installing:                                                                                                                                                                                                                                   nginx
[...]
```

## 3. Analyse

Commencez donc par démarrer le service NGINX :

```bash
[morgane@localhost ~]$ sudo dnf update
Last metadata expiration check: 0:02:36 ago on Wed 24 Nov 2021 05:00:10 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
[morgane@localhost ~]$ sudo dnf install nginx
Last metadata expiration check: 0:02:55 ago on Wed 24 Nov 2021 05:00:10 PM CET.
Dependencies resolved.
============================================================================================================================================================================================================================================= Package                                                         Architecture                               Version                                                                      Repository                                     Size =============================================================================================================================================================================================================================================Installing:                                                                                                                                                                                                                                   nginx
[...]
```

🌞 **Analysez le service NGINX**

```
[morgane@localhost ~]$ ps -ef | grep nginx
root        7947       1  0 17:06 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       7948    7947  0 17:06 ?        00:00:00 nginx: worker process
morgane     7958    5322  0 17:09 pts/0    00:00:00 grep --color=auto nginx

[morgane@localhost ~]$ ss -lntr
State                       Recv-Q                      Send-Q                                            Local Address:Port                                             Peer Address:Port                      Process
LISTEN                      0                           128                                                     0.0.0.0:80                                                    0.0.0.0:*
LISTEN                      0                           128                                                     0.0.0.0:22                                                    0.0.0.0:*
LISTEN                      0                           128                                                        [::]:80                                                       [::]:*
LISTEN                      0                           128                                                        [::]:22                                                       [::]:*

[morgane@localhost ~]$ ss -tulp
Netid                   State                    Recv-Q                   Send-Q                                      Local Address:Port                                       Peer Address:Port                   Process
tcp                     LISTEN                   0                        128                                               0.0.0.0:http                                            0.0.0.0:*
tcp                     LISTEN                   0                        128                                               0.0.0.0:ssh                                             0.0.0.0:*
tcp                     LISTEN                   0                        128                                                  [::]:http                                               [::]:*
tcp                     LISTEN                   0                        128                                                  [::]:ssh                                                [::]:*


[morgane@localhost ~]$ cat /etc/nginx/nginx.conf | grep root
root         /usr/share/nginx/html;
#        root         /usr/share/nginx/html;


[morgane@localhost html]$ ls -al
total 20
drwxr-xr-x. 2 root root   99 Nov 24 17:03 .
drwxr-xr-x. 4 root root   33 Nov 24 17:03 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```

## 4. Visite du service web

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** (c'est du TCP ;) )

```
[morgane@localhost html]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for morgane:
success

[morgane@localhost html]$ sudo firewall-cmd --reload
success
```

🌞 **Tester le bon fonctionnement du service**

```
C:\Users\33625>curl http://10.1.1.3:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
[...]
```

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

```
[morgane@localhost html]$ cat /etc/nginx/nginx.conf
[...]
 server {
   listen       8080 default_server;
   listen       [::]:8080 default_server;
 [...]

 [morgane@localhost html]$ sudo systemctl restart nginx
 [morgane@localhost html]$ sudo systemctl status nginx
 ● nginx.service - The nginx HTTP and reverse proxy server
 Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
 Active: active (running) since Wed 2021-11-24 17:33:51 CET; 15s ago
 Process: 8021 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
 Process: 8018 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
 Process: 8016 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)


 [morgane@localhost html]$ sudo firewall-cmd --remove-port=80/tcp --permanent
 success
 [morgane@localhost html]$ sudo firewall-cmd --add-port=8080/tcp --permanent
 success
 [morgane@localhost html]$ sudo firewall-cmd --reload
 success
 [...]


 C:\Users\33625> curl http://10.1.1.3:8080
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
 <head>
 <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <style type="text/css">
 [...]
```

🌞 **Changer l'utilisateur qui lance le service**

```
[morgane@localhost html]$ cd /home
[morgane@localhost home]$ ls
morgane  web

[morgane@localhost home]$ cat /etc/passwd | grep web
[...]
web:x:1001:1001::/home/web:/bin/bash


[morgane@localhost home]$ cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/
user web;
[...]

[morgane@localhost home]$ ps -ef | grep web
web         8129    8128  0 18:05 ?        00:00:00 nginx: worker process
morgane     8138    5322  0 18:07 pts/0    00:00:00 grep --color=auto web
[morgane@localhost home]$ ps -ef | grep nginx
root        8128       1  0 18:05 ?        00:00:00 nginx: master process /usr/sbin/nginx
web         8129    8128  0 18:05 ?        00:00:00 nginx: worker process
morgane     8140    5322  0 18:08 pts/0    00:00:00 grep --color=auto nginx
```

🌞 **Changer l'emplacement de la racine Web**

```
[morgane@localhost home]$ ls -al /var/www
total 4
drwxr-xr-x.  3 root root   28 Nov 24 18:12 .
drwxr-xr-x. 22 root root 4096 Nov 24 18:11 ..
drwxr-xr-x.  2 web  root   24 Nov 24 18:13 super_site_web
[morgane@localhost home]$ ls -al /var/www/super_site_web
total 4
drwxr-xr-x. 2 web  root 24 Nov 24 18:13 .
drwxr-xr-x. 3 root root 28 Nov 24 18:12 ..
-rw-r--r--. 1 web  root 28 Nov 24 18:14 index.html


[morgane@localhost home]$ cat /etc/nginx/nginx.conf | grep root
root        /var/www/super_site_web;
#        root         /usr/share/nginx/html;

C:\Users\33625> curl http://10.1.1.3:8080
<h1>toto</h1>
<h1>toto</h1>
```
